package task004;

public class NthSeries {
    public static String seriesSum(int n) {
        String result;
        if (n == 0) {
            result = "0.00";
        } else {
            int step = 3;
            float sum = 0;
            float denominator = 1;
            for (int i = 1; i <= n; i++) {
                sum = sum + 1 / denominator;
                denominator = denominator + step;
            }
            result = String.format("%.2f", sum);
        }
        return result;// Happy Coding ^_^
    }
}
