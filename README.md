# Tasks from Codewars.com

## 8 kyu

* [Given a year, return the century it is in.](src/main/java/task001)
* [Even or odd](src/main/java/task002)
* [Check for Factor](src/main/java/task003)
* [написать функцию, которая возвращает сумму следующих рядов до n-го члена (параметра).](src/main/java/task004)
